var files_dup =
[
    [ "bnosensor.py", "bnosensor_8py.html", [
      [ "BNO", "classbnosensor_1_1BNO.html", "classbnosensor_1_1BNO" ]
    ] ],
    [ "controller.py", "controller_8py.html", [
      [ "Controller", "classcontroller_1_1Controller.html", "classcontroller_1_1Controller" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "MotorDriver", "classmotor_1_1MotorDriver.html", "classmotor_1_1MotorDriver" ]
    ] ],
    [ "PIDController.py", "PIDController_8py.html", [
      [ "PID", "classPIDController_1_1PID.html", "classPIDController_1_1PID" ]
    ] ]
];